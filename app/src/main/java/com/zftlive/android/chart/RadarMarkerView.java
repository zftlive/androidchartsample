package com.zftlive.android.chart;

import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;

/**
 * Created by Java on 2017/7/2.
 */
public class RadarMarkerView extends MarkerView {

    TextView mText;

    /**
     * Constructor. Sets up the MarkerView with a custom layout resource.
     *
     * @param context
     * @param layoutResource the layout resource to use for the MarkerView
     */
    public RadarMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);
        mText = (TextView) findViewById(R.id.tv_tips_text);
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        mText.setText(e.getY()+"");
        super.refreshContent(e, highlight);
    }
}
