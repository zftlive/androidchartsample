package com.zftlive.android.chart;

import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;
import java.util.List;


/**
 * 安卓图表绘制专题-雷达图样例
 *
 * @author Ajava攻城师
 * @version 1.0
 */
public class MainActivity extends FragmentActivity {

    RadarChart mChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mChart = (RadarChart) findViewById(R.id.chart_radar);

        //标题说明文本
        Description mDescription = mChart.getDescription();
        //需要做单位换算
        mDescription.setPosition(100,100);
        mDescription.setText("《王者荣耀》玩家对战资料雷达图");
        mDescription.setTextColor(Color.RED);
        mDescription.setTextSize(20);
        mDescription.setTextAlign(Paint.Align.LEFT);

        //图表基础配置
        mChart.setBackgroundColor(Color.parseColor("#101624"));
        //禁用旋转
        mChart.setRotationEnabled(false);
        mChart.setRotationAngle(300);

        //图表骨架线条
        mChart.setWebColorInner(Color.parseColor("#66FFFFFF"));
        mChart.setWebColor(Color.parseColor("#66FFFFFF"));
        mChart.setWebLineWidth(2.5f);//dp值
        mChart.setWebLineWidthInner(1.5f);

        //图例设置
        Legend mLegend = mChart.getLegend();
        mLegend.setTextColor(Color.WHITE);
        mLegend.setTextSize(16);
        mLegend.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        mLegend.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        mLegend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);

        //点击浮层提示
        mChart.setMarker(new RadarMarkerView(this,R.layout.radar_markerview));
        mChart.setDrawMarkers(true);

        //XY基础设置
        XAxis mX = mChart.getXAxis();
        mX.setTextSize(14);
        mX.setTextColor(Color.WHITE);
        mX.setValueFormatter(new IAxisValueFormatter() {
            String labels[] = new String[]{"战绩KDA","团战","生存","输出","发育","推进"};

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return labels[(int)(value%labels.length)];
            }
        });

        YAxis mY = mChart.getYAxis();
        mY.setAxisMaximum(1.0f);
        mY.setAxisMinimum(0.0f);
        mY.setTextSize(12);
        mY.setTextColor(Color.WHITE);

        //组装XY轴数据
        List<RadarEntry> mRadarList = new ArrayList<>();
        mRadarList.add(new RadarEntry(0.45f));
        mRadarList.add(new RadarEntry(0.45f));
        mRadarList.add(new RadarEntry(0.45f));
        mRadarList.add(new RadarEntry(0.45f));
        mRadarList.add(new RadarEntry(0.45f));
        mRadarList.add(new RadarEntry(0.95f));

        //每一个玩家各个维度的数据集合
        RadarDataSet mPerDataSet = new RadarDataSet(mRadarList,"#鲁班七号#玩家");
        mPerDataSet.setDrawValues(false);//不绘制对应的点数据
        mPerDataSet.setDrawHorizontalHighlightIndicator(false);
        mPerDataSet.setDrawHighlightCircleEnabled(true);
        mPerDataSet.setDrawFilled(true);
        mPerDataSet.setFillAlpha((int)(0.4 * 255));
        mPerDataSet.setFillColor(Color.parseColor("#2c4c63"));
        mPerDataSet.setColor(Color.YELLOW);

        //雷达图数据模型
        RadarData mRadarData = new RadarData(mPerDataSet);
        mChart.setData(mRadarData);

        //渲染UI
        mChart.invalidate();
    }
}
